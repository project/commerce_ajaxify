<?php

namespace Drupal\commerce_ajaxify;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Provides #lazy_builder callbacks.
 */
class ProductLazyBuilders implements TrustedCallbackInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new CartLazyBuilders object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FormBuilderInterface $form_builder) {
    $this->entityTypeManager = $entity_type_manager;
    $this->formBuilder = $form_builder;
  }

  /**
   * Builds the add to cart form.
   *
   * @param string $product_id
   *   The product ID.
   * @param string $view_mode
   *   The view mode used to render the product.
   * @param bool $combine
   *   TRUE to combine order items containing the same product variation.
   *
   * @return array
   *   A renderable array containing the cart form.
   */
  public function ajaxAddToCartForm($product_id, $view_mode, $combine) {

    $form_object = $this->formBuilder->getForm('\Drupal\commerce_ajaxify\Form\AjaxAddRemoveCartForm', $product_id);

    return $form_object;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['ajaxAddToCartForm'];
  }

}
