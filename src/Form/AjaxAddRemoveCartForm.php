<?php

namespace Drupal\commerce_ajaxify\Form;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\ReplaceCommand;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Form to Add and Remove items from cart.
 */
class AjaxAddRemoveCartForm extends FormBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CartController object.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The cart manager.
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(AccountInterface $account, CartManagerInterface $cart_manager, CartProviderInterface $cart_provider, RequestStack $request_stack, EntityTypeManagerInterface $entity_type_manager) {
    $this->account = $account;
    $this->cartManager = $cart_manager;
    $this->cartProvider = $cart_provider;
    $this->requestStack = $request_stack;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
              $container->get('current_user'),
              $container->get('commerce_cart.cart_manager'),
              $container->get('commerce_cart.cart_provider'),
              $container->get('request_stack'),
              $container->get('entity_type.manager')
      );
  }

  /**
   * The Form Id.
   */
  public function getFormId() {
    return 'ca_add_remove_cart_form';
  }

  /**
   * The buildForm method.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $product_id = NULL) {

    $form['#attached']['library'][] = 'commerce_ajaxify/commerce_ajaxify';

    if ($this->requestStack->getCurrentRequest()->get('product_id')) {
      $product_id = (int) $this->requestStack->getCurrentRequest()->get('product_id');
    }

    $product = Product::load((int) $product_id);

    /* Load Product Variations */
    $arr_product_variation = $this->entityTypeManager->getStorage('commerce_product_variation')->loadMultiple(array_values($product->getVariationIds()));

    $list_product_variation = [];
    foreach ($arr_product_variation as $product_variation) {
      $list_product_variation[$product_variation->Id()] = $product_variation->getTitle();
    }

    $form['product_variation']['#type'] = 'select';
    $form['product_variation']['#options'] = $list_product_variation;

    $form['product_variation']['#attributes'] = [
      'class' => [
        'ca-cart-product-variation',
        'btn',
        'btn-md',
        'btn-primary',
        'use-ajax-submit',
      ],
    ];

    $form['product_variation']['#ajax'] = [
      'callback' => '::addRemoveFormCart',
    ];

    $form['product_id']['#type'] = 'hidden';
    $form['product_id']['#default_value'] = $product_id;

    $storeId = $product->get('stores')->getValue()[0]['target_id'];

    $store = $this->entityTypeManager->getStorage('commerce_store')
      ->load($storeId);

    $order = $this->cartProvider->getCart('default', $store, $this->account);

    $product_variation_id = $form_state->getValues()['product_variation'];

    if ($this->requestStack->getCurrentRequest()->get('product_variation')) {
      $product_variation_id = (int) $this->requestStack->getCurrentRequest()->get('product_variation');
    }

    $quantity = 0;
    $bool_remove = FALSE;
    if ($order) {
      $items = $order->getItems();
      foreach ($items as $item) {
        foreach ($arr_product_variation as $product_variation) {
          if ($product_variation_id) {
            if ($product_variation_id == $item->getPurchasedEntity()->Id()) {
              $quantity = (int) $item->getQuantity();
              $bool_remove = TRUE;
              break;
            }
          }
          elseif ($item->getPurchasedEntity()->Id() == $product_variation->Id()) {
            $form['product_variation']['#default_value'] = $product_variation->Id();
            $quantity = (int) $item->getQuantity();
            $bool_remove = TRUE;
            break;
          }
        }
      }
    }

    if (isset($form_state->getTriggeringElement()['#name']) && $form_state->getTriggeringElement()['#name'] == 'remove' && $quantity == 1) {
      $bool_remove = TRUE;
    }
    elseif (isset($form_state->getTriggeringElement()['#name']) && $form_state->getTriggeringElement()['#name'] == 'add' && $quantity == 0) {
      $bool_remove = TRUE;
    }

    $form['ca_cart']['cart_wrapper'] = [
      '#markup' => '',
      '#prefix' => '<div class="ca-cart-wrapper result_message-' . $product_id . '">',
    ];

    if ($bool_remove) {
      $form['ca_cart']['remove'] = [
        '#name' => 'remove',
        '#type' => 'button',
        '#value' => $this->t('Remove'),
        '#attributes' => [
          'class' => [
            'ca-cart-remove',
            'btn',
            'btn-md',
            'btn-primary',
            'use-ajax-submit',
          ],
        ],
        '#ajax' => [
          'disable-refocus' => TRUE,
          'event' => 'click',
          'callback' => '::addRemoveFormCart',
        ],
      ];
    }
    $str_quantity = ($quantity) ? $quantity : '';
    $form['ca_cart']['product_quantity'] = [
      '#type' => 'markup',
      '#markup' => '<div class="quantity">' . $str_quantity . '</div>',
    ];

    if (!$bool_remove) {
      $form['ca_cart']['product_quantity']['#prefix'] = '<div class="ca-cart-wrapper result_message-' . $product_id . '">';
    }

    $form['ca_cart']['add'] = [
      '#name' => 'add',
      '#type' => 'button',
      '#value' => $this->t('Add'),
      '#attributes' => [
        'class' => [
          'ca-cart-add',
          'btn',
          'btn-md',
          'btn-primary',
          'use-ajax-submit',
        ],
      ],
      '#ajax' => [
        'disable-refocus' => TRUE,
        'event' => 'click',
        'callback' => '::addRemoveFormCart',
      ],
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * Validate function.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Submit function.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Function to add or remove items from cart.
   */
  public function addRemoveFormCart(array $form, FormStateInterface $form_state) {

    $bool_remove = FALSE;
    $bool_add = FALSE;
    if (isset($form_state->getTriggeringElement()['#name']) && $form_state->getTriggeringElement()['#name'] == 'remove') {
      $bool_remove = TRUE;
    }
    if (isset($form_state->getTriggeringElement()['#name']) && $form_state->getTriggeringElement()['#name'] == 'add') {
      $bool_remove = FALSE;
      $bool_add = TRUE;
    }

    $product_id = $form_state->getValues()['product_id'];

    if ($this->requestStack->getCurrentRequest()->get('product_id')) {
      $product_id = (int) $this->requestStack->getCurrentRequest()->get('product_id');
    }

    $productObj = Product::load($product_id);

    $product_variation_id = $form_state->getValues()['product_variation'];

    if ($this->requestStack->getCurrentRequest()->get('product_variation')) {
      $product_variation_id = (int) $this->requestStack->getCurrentRequest()->get('product_variation');
    }

    $storeId = $productObj->get('stores')->getValue()[0]['target_id'];

    $store = $this->entityTypeManager->getStorage('commerce_store')
      ->load($storeId);

    if (!$bool_remove && $bool_add) {
      $variationobj = $this->entityTypeManager->getStorage('commerce_product_variation')
        ->load($product_variation_id);
      $cart = $this->cartProvider->getCart('default', $store);
      if (!$cart) {
        $cart = $this->cartProvider->createCart('default', $store);
      }
      $this->cartManager->addEntity($cart, $variationobj);
    }

    $order = $this->cartProvider->getCart('default', $store, $this->account);

    $items = $order->getItems();
    $total_item_in_cart = 0;
    $item_quantity = 0;

    foreach ($items as $item) {
      if ($item->getPurchasedEntity()->Id() == $product_variation_id) {
        $quantity = $item->getQuantity();
        if ($bool_remove || $bool_add) {
          if ($bool_remove) {
            $quantity--;
          }
          if ($quantity > 0) {
            $item->setQuantity($quantity);
            $this->cartManager->updateOrderItem($order, $item);
          }
          else {
            $this->cartManager->removeOrderItem($order, $item);
          }
        }
        $item_quantity = $quantity;
        $total_item_in_cart += $quantity;
        break;
      }

    }

    if (isset($form_state->getTriggeringElement()['#name']) && $form_state->getTriggeringElement()['#name'] == 'remove' && $item_quantity == 0) {
      unset($form['ca_cart']['remove']);
    }
    $response = new AjaxResponse();

    $str_quantity = ($item_quantity) ? (int) $item_quantity : '';
    $form['ca_cart']['product_quantity']['#markup'] = '<div class="quantity">' . $str_quantity . '</div>';

    $response->addCommand(
          new ReplaceCommand(
                  '.result_message-' . $product_id,
                  $form['ca_cart']
          )
      );

    $response->addCommand(
              new HtmlCommand(
                      '.cart-block--summary__count',
                      $total_item_in_cart . ' ' . $this->t('items')
              )
      );
    return $response;
  }

}
